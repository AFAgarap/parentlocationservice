package com.darth.parentlocationservice;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Locale;

/**
 * Created by darth on 7/1/17.
 */

public class SMSListener extends BroadcastReceiver {

    private static final String TAG = SMSListener.class.getSimpleName();

    // variable to hold the extracted message
    private String messageBody;

    // variable to hold the address of sender
    private String messageFrom;

    @TargetApi(19)
    @Override
    public void onReceive(Context context, Intent intent) {
        // messageBody must be cleared every time an SMS is received by the app
        messageBody = "";

        // Checks whether there is a new SMS received
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            // getMessagesFromIntent(Intent) extracts message from the received SMS
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                // gets the address of the sender
                messageFrom = smsMessage.getOriginatingAddress();
                // checks whether the sender of location update is a child
                if (ParentLocationService.childrenPhoneNumber.contains(messageFrom)) {
                    // stores the extracted message
                    messageBody += smsMessage.getMessageBody();
                    // checks if the message is a location
                    // 'location' is a constant string added to
                    // the child app's location update SMS
                    if (messageBody.contains("location")) {
                        // Adds the location address to the index
                        // of the childPhoneNumber in the ArrayList
                        // for matching/pairing purposes
                        ParentLocationService.addChildLocationAddress(
                                ParentLocationService.childrenPhoneNumber.indexOf(messageFrom),
                                messageBody);
                    }
                }
            }
        }
        Log.i(TAG, String.format(Locale.ENGLISH, "Message from %s : %s", messageFrom, messageBody));
    }
}
