package com.darth.parentlocationservice;

/**
 * Created by darth on 7/1/17.
 */

public class Constants {

    /**
     * Location request message constant to be used for sending SMS
     */
    public static final String LOCATION_REQUEST_MESSAGE = "locationUpdateRequest";
}
