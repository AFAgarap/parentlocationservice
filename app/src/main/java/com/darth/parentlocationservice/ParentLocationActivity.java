package com.darth.parentlocationservice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

public class ParentLocationActivity extends AppCompatActivity {

    private static final String TAG = ParentLocationActivity.class.getSimpleName();

    private Intent smsListener;

    // to be used for storing the
    // index of the child phone number
    private int index = Integer.parseInt(null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_location);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // stop the SMS listener when app exits
        this.stopService(smsListener);
    }

    /**
     * Call when the app will start listening to SMS updates
     */
    private void startListening() {
        smsListener = new Intent(this, SMSListener.class);
        this.startService(smsListener);
    }

    /**
     * Returns the phone number of the smart device
     * Of course, the caveat is that the user must have had
     * the phone number set first
     * @return
     */
    public String getServiceCentreNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getLine1Number();
    }

    /**
     * Event handler for a button designed to press
     * for when the parent wants to *start* listening to SMS updates
     * @param view
     */
    public void startListeningButtonHandler(View view) {
        startListening();
    }

    /**
     * Event handler for a button designed to press
     * for when the parent wants to *stop* listening to SMS updates
     * @param view
     */
    public void stopListeningButtonHandler(View view) {
        this.stopService(smsListener);
    }

    /**
     * Event handler for a button designed to press
     * for when the parent wants to request for SMS updates
     * @param view
     */
    public void sendSmsRequestButtonHandler(View view) {
        try {
            ParentLocationService.sendSMSLocationRequest(
                    ParentLocationService.getChildPhoneNumber(index),
                    getServiceCentreNumber());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
