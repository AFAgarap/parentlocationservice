package com.darth.parentlocationservice;

import android.telephony.SmsManager;

import java.util.ArrayList;

/**
 * Created by darth on 7/1/17.
 */

public class ParentLocationService {

    /**
     * ArrayList to store all phone numbers of children
     */
    protected static ArrayList<String> childrenPhoneNumber = new ArrayList<>();

    /**
     * ArrayList to store all location addresses of children
     */
    protected static ArrayList<String> childLocationAddress = new ArrayList<>();

    /**
     * Adds the received location update
     * @param childLocationAddress
     */
    public static void addChildLocationAddress(int childIndex, String childLocationAddress) {
        ParentLocationService.childLocationAddress.add(childIndex, childLocationAddress);
    }

    /**
     * Add a child phone number to list of children to be monitored
     * @param childPhoneNumber
     */
    public static void addChildPhoneNumber(String childPhoneNumber) {
        ParentLocationService.childrenPhoneNumber.add(childPhoneNumber);
    }

    /**
     * Returns the child phone number specified by the index
     * @param index
     * @return
     */
    public static String getChildPhoneNumber(int index) {
        return ParentLocationService.childrenPhoneNumber.get(index);
    }

    /**
     * Method for sending SMS location request to child app
     * @param childPhoneNumber phone number of the child app
     * @param serviceCentreNumber phone number of the device, to be used for sending SMS
     */
    public static void sendSMSLocationRequest(String childPhoneNumber, String serviceCentreNumber) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(childPhoneNumber, serviceCentreNumber, Constants.LOCATION_REQUEST_MESSAGE, null, null);
    }
}
