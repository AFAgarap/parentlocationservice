# ParentLocationService

This is the *parent* version of the [LocationService](https://bitbucket.org/AFAgarap/locationservice) app. This is designed to send a location address update request to its *child* app.

### Technical specification ###
* Minimum SDK version: API Level 19 (Android 4.4 KitKat)
* Maximum SDK version: API Level 26 (Android 8.0 O)
